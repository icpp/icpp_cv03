cmake_minimum_required(VERSION 3.15)
project(icpp_cv03)

set(CMAKE_CXX_STANDARD 14)

add_executable(icpp_cv03 Hra.cpp Hra.h Monstrum.h PohyblivyObjekt.h StatickyObjekt.h Objekt.h)